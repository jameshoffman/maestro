<?php
namespace fw;

class Router {

    public static function handle() {
        $uri = $_SERVER['REQUEST_URI'];
        $uri = explode("?", $uri)[0]; // ignore GET params

        $method = $_SERVER['REQUEST_METHOD'];
        
        $routes = include('../app/Routes.php');
        
        $action = $routes[$uri][$method] ?? NULL;
        
        if ($action) {
            [$controller, $function] = explode(':', $action);
            $controller = '\\app\\controllers\\' . $controller;
        
            call_user_func([new $controller, $function]);
        } else {
            echo('Invalid route: ' . $method . ' ' . $uri);
        }
    }

    public static function redirect($route) {
        return header("Location: {$route}");
    }
}