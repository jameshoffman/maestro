<?php
namespace fw;

use \League\Plates\Engine;

class Controller {
    protected function render($view, $parameters = []) {
        $templates = new Engine('../app/views');

        echo $templates->render($view, $parameters);
    }

    protected function params() {
        return [
            'get' => $_GET,
            'post' => $_POST,
            'files' => (count($_FILES) > 0 && reset($_FILES)['error'] != 4) ? $_FILES : []
        ];
    }

    protected function sessionSet($key, $value) {
        $_SESSION[$key] = $value;
    }

    protected function sessionClear($key) {
        $session =  $_SESSION[$key] ?? null;
        unset($_SESSION[$key]);

        return $session;
    }


}