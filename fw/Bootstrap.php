<?php
use fw\Config;
use fw\Router;

include('../fw/Autoload.php');

if (Config::get('DEBUG')) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

session_start();

Router::handle();