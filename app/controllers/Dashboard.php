<?php
namespace app\controllers;

use \app\models\AppsService;

class Dashboard extends Authenticated {
    public function index() {
        $this->authenticate();

        $apps = AppsService::all();
        
        $this->render('Dashboard', compact('apps'));
    }
}