<?php
namespace app\controllers;

use fw\Router;
use app\models\App;
use app\models\AppsService;
use app\models\DeployService;

class Deployments extends Authenticated {
    public function index() {
        $this->authenticate();

        $apps = AppsService::all();

        $error = $this->sessionClear('error');
        if (count($apps) == 0 && $error == null) {
            $error = 'No apps yet, deploy an app first!';
        }

        $this->render('Deployments', compact('apps', 'error'));
    }

    public function create() {
        $this->authenticate();

        $params = $this->params();
        $newApp = App::init($params);

        if ($newApp == null) {
            $this->sessionSet('error', 'Invalid form data<br/>' . json_encode($params));
        }

        $deployError = DeployService::deploy($newApp, $params['files']['file']['tmp_name'] ?? null);
        if ($deployError != null) {
            $this->sessionSet('error', 'Deploy error<br/>' . $deployError);
        } else {
            AppsService::add($newApp);
        }
        
        Router::redirect("/deployments");
    }

    public function view() {
        $this->authenticate();

        $app = AppsService::findById($this->params()['get']['id']);

        $this->render('DeploymentEdit', compact('app'));
    }

    public function edit() {
        $this->authenticate();

        $id = $this->params()['post']['id'];
        $scripts = [
            'update' => $this->params()['post']['updateScript'],
            'cleanup' => $this->params()['post']['cleanupScript']
        ];
        AppsService::edit($id, $scripts);

        Router::redirect("/deployments");
    }

    public function delete() {
        $this->authenticate();

        $id = $this->params()['post']['id'];

        AppsService::delete($id);

        Router::redirect("/deployments");
    }

    public function update() {
        $this->authenticate();

        $app = AppsService::findById($this->params()['post']['id']);

        DeployService::update($app);

        Router::redirect("{$this->params()['post']['redirect']}");
    }
}