<?php
namespace app\controllers;

use \fw\Controller as BaseController;
use \fw\Config;

class Authenticated extends BaseController {

    protected function authenticate() {
        $username = $_SERVER['PHP_AUTH_USER'] ?? '';
        $password = $_SERVER['PHP_AUTH_PW'] ?? '';
        $auth = "{$username}:{$password}";

        if ($auth != Config::get('ADMIN_CREDENTIALS')) { 
            header('WWW-Authenticate: Basic realm="Maestro\'s Admin"');
            header('HTTP/1.0 401 Unauthorized'); 

            echo('Enter a valid username and password.'); 
            exit;
        } 
    }
}