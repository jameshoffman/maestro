<?php

return [
    '/' => [
        'GET' => 'Dashboard:index'
    ],
    '/deployments' => [
        'GET' => 'Deployments:index',
        'POST' => 'Deployments:create'
    ],
    '/deployments/edit' => [
        'GET' => 'Deployments:view',
        'POST' => 'Deployments:edit'
    ],
    '/deployments/delete' => [
        'POST' => 'Deployments:delete'
    ],
    '/deployments/update' => [
        'POST' => 'Deployments:update'
    ]
];