<?php

namespace app\models;

use app\models\App;

class AppsService {

    public static function all() {
        $apps = [];

        if(file_exists('apps.json')) {
            $jsonString = file_get_contents("apps.json");

            if ($jsonString) {
                $appsJson = json_decode($jsonString, true);

                if ($appsJson) {
                    foreach($appsJson as $app) {
                        array_push($apps, new App($app));
                    }
                }
            }
        }

        return $apps;
    }

    public static function findById($id) {
        $apps = array_filter(AppsService::all(), function($app) use ($id) {
            return ($app->id == $id);
        });

        return reset($apps) ?? null;
    }

    public static function add($app) {
        $apps = AppsService::all();

        array_push($apps, $app);
        AppsService::save($apps);

        return $apps;
    }

    public static function edit($id, $scripts) {
        $apps = AppsService::all();

        foreach($apps as $app) {
            if ($app->id == $id) {
                $app->updateScript = $scripts['update'];
                $app->cleanupScript = $scripts['cleanup'];
            }
        }

        AppsService::save($apps);
    }

    public static function delete($id) {
        $apps = AppsService::all();

        $apps = array_filter($apps, function($app) use ($id) {
            if ($app->id == $id) {
                // Discard id to delete
                DeployService::remove($app);

                return false;
            } else {
                return true;
            }
        });
        AppsService::save($apps);

        return $apps;
    }

    private static function save($apps) {
        $file = fopen('apps.json','w');
        
        usort($apps, function($left, $right) {return strcmp($left->name, $right->name);});
        fwrite($file, json_encode($apps));
        
        fclose($file);
    }
}