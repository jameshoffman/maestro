<?php

namespace app\models;

use fw\Model;

class App extends Model {
    public static function init($params) {
        $post = $params['post'];
        $file = $params['files'];

        $instanceData = [
            'id' => uniqid(),
            'name' => $post['name'],
            'destination' => $post['destination'],
            'sourceType' => $post['sourceType'],
            'sourceData' => $post['sourceType'] == 'git' ? $post['url'] : $file['file']['name'],
            'setupScript' => $post['setupScript'] ?? '',
            'updateScript' => $post['updateScript'] ?? '',
            'cleanupScript' => $post['cleanupScript'] ?? '',
            'setupVhost' => $post['setupVhost'] ?? false,
            'documentRoot' => $post['documentRoot'] ?? null,
            'serverName' => $post['serverName'] ?? null
        ];

        return parent::init($instanceData);
    }
}