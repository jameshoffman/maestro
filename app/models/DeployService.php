<?php
namespace app\models;

use fw\Config;

class DeployService {
    
    public static function deploy($app, $uploadedTempFile) {
        exec("mkdir -p {$app->destination}");

        if ($app->sourceType == 'git') {
            exec("cd {$app->destination} && git clone {$app->sourceData} .");
        } else {
            $zip = new \ZipArchive;
            $isZip = $zip->open($uploadedTempFile);

            if ($isZip === TRUE) {
                $zip->extractTo($app->destination);
                $zip->close();
            } else {
                move_uploaded_file($uploadedTempFile, "{$app->destination}/{$app->sourceData}");
            }
        }

        if ($app->setupVhost) {
            exec("sudo -- sh -c 'cp /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/{$app->serverName}.conf'");
            
            exec("sudo -- sh -c 'sed -i 's+#ServerName+ServerName+g' /etc/apache2/sites-enabled/{$app->serverName}.conf'"); 
            exec("sudo -- sh -c 'sed -i 's+www.example.com+{$app->serverName}.lan+g' /etc/apache2/sites-enabled/{$app->serverName}.conf'"); 
            exec("sudo -- sh -c 'sed -i 's+/var/www/+{$app->documentRoot}+g' /etc/apache2/sites-enabled/{$app->serverName}.conf'"); 
            
            $serverIp = Config::get('SERVER_IP');
            exec("ssh pi@pihole.lan 'echo {$serverIp} {$app->serverName}.lan | sudo tee -a /etc/pihole/custom.list'");
            
            exec("ssh pi@pihole.lan 'pihole restartdns'");
        }
        
        exec($app->setupScript);

        if ($app->setupVhost) {
            // Restart apache at the end, after 3 seconds in background, 
            // otherwise it kills the web server and won't send response 
            exec("(sleep 3; sudo apachectl restart) > /dev/null 2>&1 &");
        }
        
        return null;
    }
    
    public static function update($app) {
        if ($app->sourceType == 'git') {
            exec("cd {$app->destination} && git pull");
        }

        exec($app->updateScript);
    }

    public static function remove($app) {
        exec($app->cleanupScript);

        exec("rm -rf {$app->destination}");

        if ($app->setupVhost) {
            exec("sudo -- sh -c 'rm /etc/apache2/sites-enabled/{$app->serverName}.conf'");

            $serverIp = Config::get('SERVER_IP');
            exec("ssh pi@pihole.lan 'sudo sed -i '/{$serverIp}[[:space:]]{$app->serverName}.lan/d' /etc/pihole/custom.list'");
        
            exec("ssh pi@pihole.lan 'pihole restartdns'");

            // Restart apache at the end, after 3 seconds in background, 
            // otherwise it kills the web server and won't send response 
            exec("(sleep 3; sudo apachectl restart) > /dev/null 2>&1 &");
        }
    }

}
