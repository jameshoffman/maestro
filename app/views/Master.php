<html>
<head>
    <title><?=$this->e($title)?></title>

    <link rel="stylesheet" href="/assets/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<h1 style="text-align: center;">🤵  <a href="/">Maestro</a></h1>

<?php if(isset($error) && $error) { ?>
    <div style="border-radius: 3px; border: 1px solid darkred; word-break: break-word; background-color: lightpink; color: darkred; padding: 10px;">
        <?= $error ?>
    </div>
<?php } ?>

<?=$this->section('content')?>

</body>
</html>