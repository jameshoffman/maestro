<?php $this->layout('Master', ['title' => 'Deployments', 'error' => $error]) ?>

<h2>Deployments</h2>                                                                                                                                                                                              

<details <?= count($apps) == 0 ? 'open' : ''?> >
    <summary>
        <span style="color: green; cursor:pointer;">Deploy an app</span>
    </summary>

    <div style="padding: 10px 0px;">
        <form action="/deployments" method="POST" enctype="multipart/form-data">
            <input class="inputRow" type="text" name="name" placeholder="Name" required>
            <input class="inputRow" type="text" name="destination" placeholder="Destination Path, ex: /var/www/name" required>

            Source
            <div class="inputRow">
                <input type="radio" style="margin-left: 0px;" id="radioGit" name="sourceType" value="git" data-input-id="inputRepoUrl" checked>
                <label for="radioGit">Git repo</label>
                <input type="radio" id="radioFile" name="sourceType" value="file" data-input-id="inputFile">
                <label for="radioFile">File upload<span style="color:grey">(Single file or ZIP)</span</label>
            </div>
            <input class="inputRow sourceInput" type="text" placeholder="Repo URL" name="url" id="inputRepoUrl" required>
            <input class="inputRow sourceInput" style="display:none;" type="file" name="file" id="inputFile">
            <script>
                document.querySelectorAll('input[name="sourceType"]').forEach((radio) => {
                    radio.onchange = function(event) {
                        document.querySelectorAll('.sourceInput').forEach((input) => {
                            input.style.display = 'none';
                            input.removeAttribute('required');
                        });

                        const input = document.getElementById(event.target.dataset.inputId);
                        input.style.display = 'block';
                        input.setAttribute('required', "");
                    };
                });
            </script>

            <details class="inputRow">
                <summary>
                    <span style="cursor:pointer;">Setup script</span>
                </summary>

                <textarea style="width: 100%; resize: vertical;" name="setupScript" rows="5"></textarea>
            </details>

            <details class="inputRow">
                <summary>
                    <span style="cursor:pointer;">Update script</span>
                </summary>

                <textarea style="width: 100%; resize: vertical;" name="updateScript" rows="5"></textarea>
            </details>

            <details class="inputRow">
                <summary>
                    <span style="cursor:pointer;">Cleanup script</span>
                </summary>

                <textarea style="width: 100%; resize: vertical;" name="cleanupScript" rows="5"></textarea>
            </details>

            <div class="inputRow">
                <input style="margin-left: 0px;" type="checkbox" name="setupVhost" id="inputVhost">
                <label for="inputVhost">Setup VHost and local DNS?</label>
            </div>
            <input class="inputRow vhostInput" style="display:none;" type="text" name="documentRoot" data-ref="destination" placeholder="Document Root, ex: /var/www/name">
            <input class="inputRow vhostInput" style="display:none;" type="text" name="serverName"  data-ref="name" placeholder="Server Name, automatically uses .lan TLD, ex: name">
            <script>
                document.getElementById('inputVhost').onchange = function(event) {
                    document.querySelectorAll('.vhostInput').forEach((input) => {
                        if (event.target.checked) {
                            input.style.display = 'block';
                            input.setAttribute('required', "");

                            input.value = document.querySelector(`input[name=${input.dataset.ref}]`).value;
                        } else {
                            input.style.display = 'none';
                            input.removeAttribute('required');
                        }
                    });
                };

            </script>
            
            <button class="success" style="float: right;">Deploy!</button>
            <div style="clear: both;"></div>
        </form>
    </div>
</details>

<?php if(count($apps) > 0) { ?>
<div style="margin-top: 20px;">

    <hr>

    <?php foreach($apps as $app) {?>
        <div class="hover" style="margin: -9px 0px; padding: 16px 8px; word-break: break-word;">
            <div style="margin-bottom: 10px; ">
                <div style="font-weight: bold; float:left;"><?=$app->name?></div>
                <form action="/deployments/delete" method="POST" onsubmit="return confirm('Are you sure?');">
                    <input type="text" name="id" value="<?=$app->id?>" hidden>
                    <button title="Delete" class="danger" style="float:right; width: 30px; font-size: 20px;">X</button>
                </form>

                <form action="/deployments/edit" method="GET">
                    <input type="text" name="id" value="<?=$app->id?>" hidden>
                    <button title="Edit" class="success" style="float:right; margin-right: 16px; width: 30px; font-size: 20px;">#</button>
                </form>

                <form action="/deployments/update" method="POST">
                    <input type="text" name="id" value="<?=$app->id?>" hidden>
                    <input type="text" name="redirect" value="/deployments" hidden>
                    <button title="Update" class="warning" style="float:right; margin-right: 16px; width: 30px; font-size: 36px;">*</button>
                </form>

                
                <div style="clear: both;"></div>
            </div>
            
            <?php if($app->setupVhost) { ?>
            <div style="margin-bottom: 10px;">
                <a href="http://<?=$app->serverName ?>.lan" target="_blank"><?=$app->serverName?>.lan</a>
                <span style="color: grey; font-style: italic;">serves</span>
                <?=$app->documentRoot?>
            </div>
            <?php } ?>

            <div style="margin-bottom: 10px;">
                <?=$app->sourceData?>
                <span style="color: grey; font-style: italic;">at</span>
                <?=$app->destination?>
            </div>

            <?php if($app->setupScript) { ?>
            <details style="margin-bottom: 10px;">
                <summary>
                    <span style="cursor:pointer;">Setup script</span>
                </summary>
                <pre style="font-family: monospace; margin: 0px; background-color: #eaeaea; overflow-y: auto; padding: 4px;"><?=$app->setupScript?></pre>
            </details>
            <?php } ?>

            <?php if($app->updateScript) { ?>
            <details style="margin-bottom: 10px;">
                <summary>
                    <span style="cursor:pointer;">Update script</span>
                </summary>
                <pre style="font-family: monospace; margin: 0px; background-color: #eaeaea; overflow-y: auto; padding: 4px 0px;"><?=$app->updateScript?></pre>
            </details>
            <?php } ?>

            <?php if($app->cleanupScript) { ?>
            <details style="margin-bottom: 10px;">
                <summary>
                    <span style="cursor:pointer;">Cleanup script</span>
                </summary>
                <pre style="font-family: monospace; margin: 0px; background-color: #eaeaea; overflow-y: auto; padding: 4px 0px;"><?=$app->cleanupScript?></pre>
            </details>
            <?php } ?>
        </div>

        <hr>
    <?php } ?>

</div>
<?php } ?>