<?php $this->layout('Master', ['title' => 'Maestro']) ?>

<div>
    <div style="margin-bottom: 24px;">
        <h2 style="display: inline;">Apps</h2>
    
        <a href="/deployments" style="font-style: italic;">Deployments</a>
    </div>

    <div id="dashboardAppsGrid" style="display: grid; grid-template-columns: repeat(auto-fit, minmax(200px, 1fr)); grid-gap: 1rem;">
        <?php foreach($apps as $app) {?>
            <div class="hover appCard" 
            style="text-align: center; padding: 16px;">
                <div class="name no-select" 
                style="font-size: 20px; margin-bottom: 16px; 
                white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                    <?=$app->name?>
                </div>
    
                <form action="/deployments/update" method="POST">
                    <input type="text" name="id" value="<?=$app->id?>" hidden>
                    <input type="text" name="redirect" value="/" hidden>
                    <button class="warning">Update</button>
                </form>
            </div>
        <?php } ?>
    </div>
</div>
