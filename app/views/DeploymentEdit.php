<?php $this->layout('Master', ['title' => 'Edit']) ?>

<h2><?=$app->name?></h2>

<?php if($app->setupVhost) { ?>
<div style="margin-bottom: 10px;">
    <a href="http://<?=$app->serverName ?>.lan" target="_blank"><?=$app->serverName?>.lan</a>
    <span style="color: grey; font-style: italic;">serves</span>
    <?=$app->documentRoot?>
</div>
<?php } ?>

<div style="margin-bottom: 10px;">
    <?=$app->sourceData?>
    <span style="color: grey; font-style: italic;">at</span>
    <?=$app->destination?>
</div>

<form id="editForm" action="/deployments/edit" method="POST">
    <input type="text" name="id" value="<?=$app->id?>" hidden>

    <?php if($app->setupScript) { ?>
    <details class="inputRow" <?= $app->setupScript ? 'open' : ''?>>
        <summary>
            <span style="cursor:pointer;">Setup script</span>
        </summary>

        <textarea style="width: 100%; resize: vertical; background-color: lightgrey" name="setupScript" rows="5" readonly><?=$app->setupScript?></textarea>
    </details>
    <?php }?>

    <details class="inputRow" <?= $app->updateScript ? 'open' : ''?>>
        <summary>
            <span style="cursor:pointer;">Update script</span>
        </summary>

        <textarea style="width: 100%; resize: vertical;" name="updateScript" rows="5"><?=$app->updateScript?></textarea>
    </details>

    <details class="inputRow" <?= $app->cleanupScript ? 'open' : ''?>>
        <summary>
            <span style="cursor:pointer;">Cleanup script</span>
        </summary>

        <textarea style="width: 100%; resize: vertical;" name="cleanupScript" rows="5"><?=$app->cleanupScript?></textarea>
    </details>
</form>
<form action="/deployments/delete" method="POST" onsubmit="return confirm('Are you sure?');">
    <input type="text" name="id" value="<?=$app->id?>" hidden>
    <button class="danger" style="float: left; margin-right: 24px;">Delete</button>
</form>
<form action="/deployments/update" method="POST">
    <input type="text" name="id" value="<?=$app->id?>" hidden>
    <input type="text" name="redirect" value="/deployments" hidden>
    <button id="updateButton" class="warning" style="float: left;">Update</button>
</form>
<script>
document.querySelectorAll('textarea').forEach((textarea) => {
    textarea.oninput = function(evt) {
        input = evt.target;
        updateButton = document.querySelector('#updateButton');

        if (input.value != input.defaultValue) {
            updateButton.setAttribute('disabled', "");
        } else {
            updateButton.removeAttribute('disabled');
        }
    }
});
</script>


<button form="editForm" class="success" style="float: right;">Save</button>
<button form="editForm" type="reset" style="float: right; margin-right: 24px;" onclick="document.querySelector('#updateButton').removeAttribute('disabled');">Cancel</button>