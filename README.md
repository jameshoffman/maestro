# Maestro

## Setup

- Create config file
```
cp app/Config.example.php app/Config.php 
```
- Edit values in `app/Config.php`

## Dev

- Start dev server
```
cd webroot && php -S 0.0.0.0:8080
```
- Test deployment setup using the Vagrant VM
```
vagrant up
vagrant halt
```

## Deploy

- Look at the setup invagrantfile starting at line 20 for complete server setup

## References

- Templating: <http://platesphp.com/>
- PHP OPP: <http://www.phpfreaks.com/tutorial/oo-php-part-1-oop-in-full-effect>